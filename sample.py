from pymongo import MongoClient

uri = "mongodb://user:password@127.0.0.1:27017/guestbook"
client = MongoClient(uri)

db = client.get_database("guestbook")
collection = db.get_collection("test")
collection.insert_one({'test': 'toto'})
print(collection.find_one({'test': 'toto'}))